import React from 'react';
import './App.css';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Homepage from './components/Homepage';
import Login from './components/Login';
import Register from './components/Register';
import ForgotPassword from './components/ForgotPassword';
import ResetPassword from './components/ResetPassword';
import Movies from './components/Movies';
import AddMovie from './components/AddMovie';
import Movie from './components/Movie';
import AddSeries from './components/AddSeries';
import AllSeries from './components/AllSeries';
import Series from './components/Series';
import Team from './components/Team';
import Verify from './components/Verify';
import Profile from './components/Profile';
import WrapperComponent from './components/WrapperComponent';
import SendMailMovie from './components/SendMailForMovie';
import SendMailSeries from './components/SendMailForSeries';
import WrapperSecretComponentSeries from './components/WrapperSecretComponentSeries';
import WrapperSecretComponent from './components/WrapperSecretComponent';
import WrapperSecretComponentMovie from './components/WrapperSecretComponentMovie';

function PrivateRoute ({component: Component, ...rest}) {
  return (
    <Route {...rest} render={props => (
      <WrapperComponent {...props}/>
    )}/>
  )
}

function SecretRoute ({component: Component, ...rest}) {
  return (
    <Route {...rest} render={props => (
      <WrapperSecretComponent {...props}/>
    )}/>
  )
}

function PrivateRouteForAddMovie ({component: Component, ...rest}) {
  return (
    <Route {...rest} render={props => (
      <WrapperSecretComponentMovie {...props}/>
    )}/>
  )
}

function PrivateRouteForAddSeries ({component: Component, ...rest}) {
  return (
    <Route {...rest} render={props => (
      <WrapperSecretComponentSeries {...props}/>
    )}/>
  )
}

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" exact component={Homepage} />
        <Route path="/login" exact component={Login} />
        <Route path="/register" exact component={Register} />
        <Route path="/forgotPassword" exact component={ForgotPassword} />
        <SecretRoute path="/resetPassword/:token" exact component={ResetPassword} />
        <Route path="/movies" exact component={Movies} />
        <Route path="/series" exact component={AllSeries} />
        <PrivateRouteForAddMovie path="/addMovie" exact component={AddMovie} />
        <PrivateRouteForAddSeries path="/addSeries" exact component={AddSeries} />
        <Route path="/movie/:name" exact component={Movie} />
        <Route path="/series/:name" exact component={Series} />
        <Route path="/verify" exact component={Verify} />
        <PrivateRoute path="/profile" exact component={Profile} />
        <Route path="/sendMailMovie" exact component={SendMailMovie} />
        <Route path="/sendMailSeries" exact component={SendMailSeries} />
        <Route path="/team" exact component={Team} />


      </Switch>
    </BrowserRouter>
  );
}

export default App;