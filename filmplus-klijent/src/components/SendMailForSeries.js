import React, { Component } from 'react';
import axios from 'axios';
import '../css/SendMailForSeries.css';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAngleLeft } from "@fortawesome/free-solid-svg-icons";

export default class SendMailSeries extends Component {
    constructor(props) {
        super(props);
        this.state = {
          title: '',
          html: ''
      };

      this.onTitleChange = this.onTitleChange.bind(this);
      this.onHtmlChange = this.onHtmlChange.bind(this);
      this.sendMail = this.sendMail.bind(this);
    }

    onTitleChange(title) {
        this.setState({
            title: title.target.value
        });
    }

    onHtmlChange(html) {
        this.setState({
            html: html.target.value
        });
    }

    sendMail() {
        const body = {
            title: this.state.title,
            html: this.state.html,
            email: localStorage.getItem("email")
        };

        axios.post('http://localhost:3001/sendMailForSeries', body)
            .then((_response) => {
                window.alert("Hvala što ste predložili seriju");
            })
            .catch((error) => {
                console.log(error);
            });
        document.getElementById("sendMailForSeries").reset();
    }

    render() {
        return (
        <div className="sendMailSeries">
            <div className="formular">

                <div className="back"><a href='/profile'><FontAwesomeIcon icon={faAngleLeft} className="fi_menu"/> Vratite se na profil</a></div>
                <div className="container">

                    <h2 className="titleFormLogin">Predložite nam seriju</h2>
                    <form id="sendMailForSeries">
                        <div className="form-group">
                            <label htmlFor="titleMovie">Naslov serije:</label>
                            <input type="text" className="form-control" id="titleMovie" placeholder="Unesite naslov" name="titleMovie" onChange={this.onTitleChange}/>
                        </div>

                        <div className="form-group">
                            <label htmlFor="htmlMovie">Opis serije:</label>
                            <textarea className="form-control" id="htmlMovie" placeholder="Unesite opis filma" name="htmlMovie" onChange={this.onHtmlChange} rows="3"></textarea>
                        </div>

                        <button type="button" onClick={this.sendMail} id="sendMailMovie" className="btn btn-success">Pošaljite</button>
                    </form>
                </div>
            </div>
        </div>
    );
  }
}