import React, { Component } from 'react';
import '../css/Register.css';
import axios from 'axios';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAngleLeft } from "@fortawesome/free-solid-svg-icons";

class Register extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            firstName: '',
            lastname: '',
            password: '',
            confirmPassword: '',
            image: null,
        };
    
        this.onEmailChange = this.onEmailChange.bind(this);
        this.onPasswordChange = this.onPasswordChange.bind(this);
        this.onImageChange = this.onImageChange.bind(this);
        this.onPasswordConfirmChange = this.onPasswordConfirmChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    
    checkFormValidity(entity) {
        const validationEmailRegex = new RegExp("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$");
        const validationNameRegex = new RegExp("[a-zA-Z]+");
        const validationPasswordRegex = new RegExp("^(?=.*\\d).{4,12}$");

        if (entity.email === '' || !validationEmailRegex.test(entity.email)) {
            window.alert("Please insert valid email!");
            return false;
        } else if (entity.name === '' || !validationNameRegex.test(entity.name)) {
            window.alert("Name must contain only letters!");
            return false;
        } else if (entity.lastname === '' || !validationNameRegex.test(entity.lastname)) {
            window.alert("Last name must contain only letters!");
            return false;
        } else if (entity.password === '' || !validationPasswordRegex.test(entity.password)) {
            window.alert("Password must contain between 4-12 characters and at least one digit!");
            return false;
        } else if (entity.password !== entity.confirmPassword) {
            window.alert("Passwords do not match!");
            return false;
        }

        return true;
    }

    onEmailChange(email) {
        this.setState({
            email: email.target.value
        });
    }

    onNameChange(nameOfInput, input) {
        if (nameOfInput === "name") {
            this.setState({
                firstName: input.target.value
            });

        } else if (nameOfInput === "lastname") {
            this.setState({
                lastname: input.target.value
            });
        }
    }

    onPasswordChange(password) {
        this.setState({
            password: password.target.value
        });
    }

    onPasswordConfirmChange(passwordConfirm) {
        this.setState({
            confirmPassword: passwordConfirm.target.value
        });
    }

    onImageChange(event) {
        let file = event.target.files[0];
        this.setState({image: file});
    }

    handleSubmit() {
        const forUser = {
            name: this.state.firstName,
            lastname: this.state.lastname,
            email: this.state.email,
            password: this.state.password,
            confirmPassword: this.state.confirmPassword
        };
        if (!this.checkFormValidity(forUser)) {
            return;
        }

        const user = {
            name: this.state.firstName,
            lastname: this.state.lastname,
            email: this.state.email,
            password: this.state.password
        };

        axios.post('http://localhost:3001/sendVerifyToken', {
            email: user.email
        })
        .then(_res => {
        })
        .catch(err => {
            console.log(err);
        })

        let data = new FormData();
        localStorage.setItem("email", user.email);

        data.append('image', this.state.image);
        data.append('name', this.state.firstName);
        data.append('lastname', this.state.lastname);
        data.append('email', this.state.email);
        data.append('password', this.state.password);
        axios.post('http://localhost:3001/newUser', data)
        .then(response => 
            window.alert(response.data.msg)
        )
        .catch(err => {
            console.log(err);
        })

        document.getElementById("formUser").reset();
        window.location.href = "/verify";
    }

    render() {

        return (
            <div className="register">
                <div className="formular">
                    <div className="back"><a href='/'><FontAwesomeIcon icon={faAngleLeft} className="fi_menu"/> Vratite se na početnu</a></div>
                    
                    <div className="container" id="registerUser">
                        <h2 className="titleForm">Registrujte se</h2>
                        <form id="formUser">
                            <div className="form-group">
                                <label htmlFor="email">Email:</label>
                                <input type="email" className="form-control" id="emailUser" placeholder="Unesite email" name="email" onChange={this.onEmailChange}/>
                            </div>

                            <div className="form-group">
                                <label htmlFor="name">Ime:</label>
                                <input type="text" className="form-control" id="nameUser" placeholder="Unesite ime" name="name" onChange={this.onNameChange.bind(this, "name")}/>
                            </div>

                            <div className="form-group">
                                <label htmlFor="lastname">Prezime:</label>
                                <input type="text" className="form-control" id="lastnameUser" placeholder="Unesite prezime" name="lastname" onChange={this.onNameChange.bind(this, "lastname")}/>
                            </div>

                            <div className="form-group">
                                <label htmlFor="pwd">Šifra:</label>
                                <input type="password" className="form-control" id="pwdUser" placeholder="Unesite šifru" name="pwd" onChange={this.onPasswordChange}/>
                            </div>

                            <div className="form-group">
                                <label htmlFor="pwd">Potvrdite šifru:</label>
                                <input type="password" className="form-control" id="pwdUserConfirm" placeholder="Unesite šifru" name="pwd" onChange={this.onPasswordConfirmChange}/>
                            </div>

                            <div className="form-group">
                                <label htmlFor="image">Fotografija:</label> <br/>
                                <input type="file" onChange={this.onImageChange} className="" id="group_image"/>
                            </div>

                            <button type="button" onClick={this.handleSubmit} id="submit" className="btn btn-success">Kreirajte nalog</button>
                            <div className="goToLogin"><p>Već imate nalog? <a href='/login'>Prijavite se!</a></p></div>
                        </form>
                    </div>
                </div>
            </div>
    );
  }
}

export default Register;