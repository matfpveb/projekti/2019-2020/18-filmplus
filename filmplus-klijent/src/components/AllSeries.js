import React from 'react';
import Header from './Header';
import '../css/AllSeries.css'
import axios from 'axios';
import Footer from './Footer';

class AllSeries extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            series : [],
            genres: [],
            genre: 'no',
            directors: [],
            director: 'no',
            yearStart: '1900',
            yearEnd: '2021',
            name: '',
            actor: ''
        };
        this.handleGenre = this.handleGenre.bind(this);
        this.handleDirector = this.handleDirector.bind(this);
        this.handleYearStart = this.handleYearStart.bind(this);
        this.handleYearEnd = this.handleYearEnd.bind(this);
        this.handleName = this.handleName.bind(this);
        this.handleActor = this.handleActor.bind(this);

        this.find = this.find.bind(this);
        this.findSeriesByName = this.findSeriesByName.bind(this);
        this.findSeriesByActor = this.findSeriesByActor.bind(this);

        this.showSeries = this.showSeries.bind(this);
        this.showDetailedSearch = this.showDetailedSearch.bind(this);

    }

    showDetailedSearch() {
        let filters = document.getElementById("filters");
        if (filters.style.display === "none") {
            filters.style.display = "block";
        } else {
            filters.style.display = "none";
        }
    }

    showSeries(allSeries) {
        let series = [];
        let divide = allSeries.length / 3;
        let divideInt = (allSeries.length / 3) >> 0;
        let numOfRows = 0;
        if (divide === divideInt) {
            numOfRows = divideInt;
        } else {
            numOfRows = divideInt + 1;
        }

        let k = 0;
        for (let i = 0; i < numOfRows; i++) {
            let series_row = [];
            if (allSeries.length - k >= 3) {
                for (let j = 0; j < 3; j++) {
                    let averageMarks = 0.0;
                    for (let i = 0; i < allSeries[k].rating_list.length; i++) {
                        averageMarks += Number(allSeries[k].rating_list[i].mark);
                    }
                    averageMarks = averageMarks / allSeries[k].rating_list.length;
                    let pathToImage = '/' + allSeries[k].image;
                    let pathToDetails = '/series/'+allSeries[k].name;
                    series_row.push(<div key={k} className="series_block">
                                        <div className="seria">
                                            <h3> {allSeries[k].name} </h3>
                                                <img src={pathToImage} alt="series_photo" />
                                                <a href={pathToDetails}><button className="btn btn-primary"> Pogledajte više </button></a>
                                                <p>{averageMarks}</p>
                                        </div>
                                    </div>
                                    );
                    k = k + 1;
                                
                }
            } else {
                let other = k;
                for (let j = 0; j < allSeries.length - other; j++) {
                    let averageMarks = 0;
                    for (let i = 0; i < allSeries[k].rating_list.length; i++) {
                        averageMarks += Number(allSeries[k].rating_list[i].mark);
                    }

                    averageMarks = averageMarks / allSeries[k].rating_list.length;
                    
                    let pathToImage = '/' + allSeries[k].image;
                    let pathToDetails = '/series/' + allSeries[k].name;
                    series_row.push(<div key={k} className="series_block">
                                        <div className="seria">
                                            <h3> {allSeries[k].name} </h3>
                                                <img src={pathToImage} alt="series_photo" />
                                                <a href={pathToDetails}><button className="btn btn-primary"> Pogledajte više </button></a>
                                                <p>{averageMarks}</p>
                                        </div>
                                    </div>
                                    );
                    k = k + 1;
                                
                }
            }
            series.push(<div key={i} className="series_list">{series_row}</div>);

        }
       return series;
    }

    componentDidMount() {
        axios.get(`http://localhost:3001/serieslistOfGenre/`)
        .then(res => {
            this.setState({
                genres: res.data
            });
        })
        .catch((error) => {
            console.log(error);
        });

        axios.get(`http://localhost:3001/serieslistOfDirectors/`)
        .then(res => {
            this.setState({
                directors: res.data
            });
        })
        .catch((error) => {
            console.log(error);
        });

        axios.get(`http://localhost:3001/series/`)
        .then(res => {
            let series = this.showSeries(res.data);
            this.setState({series});
        })
        .catch(_err => {
            let page = document.getElementById("series");
            page.style.height = "100vh";
        });

    }

    handleGenre(genre) {
        this.setState({
            genre: genre.target.value
        });
    }

    handleDirector(director) {
        this.setState({
            director: director.target.value
        });
    }

    handleYearStart(year) {
        this.setState({
            yearStart: year.target.value
        });
    }

    handleYearEnd(year) {
        this.setState({
            yearEnd: year.target.value
        });
    }

    handleName(name) {
        this.setState({
            name: name.target.value
        });
    }

    handleActor(actor) {
        this.setState({
            actor: actor.target.value
        });
    }

    find() {
        let director = this.state.director;
        let genre = this.state.genre;
        let yearStart = this.state.yearStart;
        let yearEnd = this.state.yearEnd;
        let footer = document.getElementById("footer");
        let page = document.getElementById("series");

        if (genre === 'no' && yearStart === '1900' && yearEnd === '2021') {
            axios.get(`http://localhost:3001/seriesByDirector/` + director)
            .then(res => {
                let series = this.showSeries(res.data);
                footer.style.position = "relative";
                page.style.height = "auto";
                this.setState({series});
            })
            .catch(_err => {
                this.setState({
                    series: []
                });
                page.style.height = "100vh";
                footer.style.position = "absolute";
                footer.style.bottom = "0px";
            });
        }
        else if (director === 'no' && yearStart === '1900' && yearEnd === '2021') {
            axios.get(`http://localhost:3001/seriesByGenre/` + genre)
            .then(res => {
                let series = this.showSeries(res.data);
                footer.style.position = "relative";
                page.style.height = "auto";
                this.setState({series});
            })
            .catch(_err => {
                this.setState({
                    series: []
                });
                page.style.height = "100vh";
                footer.style.position = "absolute";
                footer.style.bottom = "0px";
            });
        } else {
            const body = {
                genre: genre,
                director: director,
                yearStart: yearStart,
                yearEnd: yearEnd
            }
            axios.post('http://localhost:3001/listOfSeries/', body)
            .then(res => {
                let series = this.showSeries(res.data);
                this.setState({series});
                footer.style.position = "relative";
                page.style.height = "auto";

            })
            .catch(_err => {
                this.setState({
                    series: []
                });
                page.style.height = "100vh";
                footer.style.position = "absolute";
                footer.style.bottom = "0px";
            });
        }
    }

    findSeriesByName() {
        let name = this.state.name;
        let footer = document.getElementById("footer");
        let page = document.getElementById("series");
        if (name === '') {
            name = 'no';
        }
        axios.get(`http://localhost:3001/seriesByName/` + name)
        .then(res => {
            let series = this.showSeries(res.data);
            footer.style.position = "relative";
            page.style.height = "auto";
            this.setState({
                series: series,
                name: ''
            });
        })
        .catch(_err => {
            this.setState({
                series: []
            });
            page.style.height = "100vh";
            footer.style.position = "absolute";
            footer.style.bottom = "0px";
        });
    }

    findSeriesByActor() {
        let actor = this.state.actor;
        let footer = document.getElementById("footer");
        let page = document.getElementById("series");
        if (actor === '') {
            actor = 'no';
        }
        axios.get(`http://localhost:3001/seriesByActor/` + actor)
        .then(res => {
            let series = this.showSeries(res.data);
            footer.style.position = "relative";
            page.style.height = "auto";
            this.setState({
                series: series,
                name: ''
            });
        })
        .catch(_err => {
            this.setState({
                series: []
            });
            page.style.height = "100vh";
            footer.style.position = "absolute";
            footer.style.bottom = "0px";
        });
    }

    render() {
        let optionsGenre = [];
        for (let i = 0; i<this.state.genres.length; i++) {
            optionsGenre.push(<option key={i} value={this.state.genres[i]}>{this.state.genres[i]}</option>)
        }

        let optionsDirector = [];
        for (let i = 0; i<this.state.directors.length; i++) {
            optionsDirector.push(<option key={i} value={this.state.directors[i]}>{this.state.directors[i]}</option>)
        }
        return (
        <div className="series" id="series">
            <Header />
            <div className="allSeries">
                <div>
                    <h2 className="titleSeries">SERIJE</h2>
                </div>
                <button type="button" onClick={this.showDetailedSearch} id="detailedSearch" className="btn btn-success">Detaljna pretraga</button>

                <div id="filters">
                    <div id="firstFilter">
                        <div className="form-group" id="optionsList">
                            <label htmlFor="sel1">Izaberite žanr:</label>
                            <select className="form-control" id="sel1" onChange={this.handleGenre.bind(this)}>
                                <option value='no'>Niste izabrali žanr</option>
                                {optionsGenre}
                            </select>
                        </div>
                        <div className="form-group" id="optionsList2">
                            <label htmlFor="sel2">Izaberite režisera:</label>
                            <select className="form-control" id="sel2" onChange={this.handleDirector.bind(this)}>
                                <option value='no'>Niste izabrali režisera</option>
                                {optionsDirector}
                            </select>
                        </div>
                        <div className="form-group" id="yearList">
                            <label htmlFor="yearStart">Unesite godine:</label>  
                            <div className="years">
                                <input type="text" className="form-control" id="yearStart" placeholder="Od" name="yearStart" onChange={this.handleYearStart}/>
                                <input type="text" className="form-control" id="yearEnd" placeholder="Do" name="yearEnd" onChange={this.handleYearEnd}/>
                            </div>
                        </div>
                        <div>
                            <button type="button" onClick={this.find} id="find" className="btn btn-success">Pretražite</button>
                        </div>
                    </div>
                    <div id="secondFilter">
                        <div className="labelForFilter">
                            <label htmlFor="nameFilter">Pretražite po nazivu serije:</label>  
                            <input type="text" className="form-control" id="nameFilter" placeholder="Unesite naziv serije" name="nameFilter" onChange={this.handleName}/>
                        </div>
                        <div>
                            <button type="button" onClick={this.findSeriesByName} id="findSeriesByName" className="btn btn-success">Pretražite</button>
                        </div>
                    </div>
                    <div id="thirdFilter">
                        <div className="labelForFilter">
                            <label htmlFor="actorFilter">Pretražite po glumcima:</label>  
                            <input type="text" className="form-control" id="actorFilter" placeholder="Unesite ime glumca" name="actorFilter" onChange={this.handleActor}/>
                        </div>
                        <div>
                            <button type="button" onClick={this.findSeriesByActor} id="findSeriesByActor" className="btn btn-success">Pretražite</button>
                        </div>
                    </div>
                </div>
                {this.state.series}
            </div>
            <Footer />
        </div>
        );
    }

}

export default AllSeries;