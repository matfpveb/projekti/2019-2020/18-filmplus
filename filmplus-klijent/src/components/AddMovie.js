import React, { Component } from 'react';
import axios from 'axios';
import '../css/AddMovie.css';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAngleLeft } from "@fortawesome/free-solid-svg-icons";

class AddMovie extends Component {

    constructor(props) {
        super(props);
        this.state = {
            name: '',
            genre: '',
            year: '',
            runningTime: '',
            director: '',
            cast: '',
            scriptwriters: '',
            image: null,
            producers: '',
            logline: '',
        };

        this.onImageChange = this.onImageChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(nameOfInput, event) {
        if (nameOfInput === 'name') {
            this.setState({name: event.target.value});
        } else if (nameOfInput === 'genre') {
            this.setState({genre: event.target.value});
        } else if (nameOfInput === 'year') {
            this.setState({year: event.target.value});
        } else if (nameOfInput === 'runningTime') {
            this.setState({runningTime: event.target.value});
        } else if (nameOfInput === 'director') {
            this.setState({director: event.target.value});
        } else if (nameOfInput === 'cast') {
            this.setState({cast: event.target.value});
        } else if (nameOfInput === 'scriptwriters') {
            this.setState({scriptwriters: event.target.value});
        } else if (nameOfInput === 'producers') {
            this.setState({producers: event.target.value});
        } else if (nameOfInput === 'logline') {
            this.setState({logline: event.target.value});
        }
    }

    onImageChange(event) {
        let file = event.target.files[0];
        this.setState({image: file});
    }

    handleSubmit() {
        let data = new FormData();

        data.append('image', this.state.image);
        data.append('name', this.state.name);
        data.append('genre', this.state.genre);
        data.append('year', this.state.year);
        data.append('runningTime', this.state.runningTime);
        data.append('cast', this.state.cast);
        data.append('director', this.state.director);
        data.append('scriptwriters', this.state.scriptwriters);
        data.append('producers', this.state.producers);
        data.append('logline', this.state.logline);
        axios.post('http://localhost:3001/saveMovie', data)
        .then((response) => {
            window.alert(response.data);             
        })
        .catch(err => {
            console.log(err);
        });
    }
    
    render() {
        return (
             <div className="addMovie">
                <div className="formular">
                    <div className="back"><a href='/'><FontAwesomeIcon icon={faAngleLeft} className="fi_menu"/> Vratite se na početnu</a></div>
                    
                    <div className="container" id="registerUser">
                        <h2 className="titleMovie">Dodavanje filmova</h2>
                        <form id="formMovie">
                            <div className="form-group">
                                <label htmlFor="name">Naziv:</label>
                                <input type="text" className="form-control" id="name" placeholder="Unesite naziv" name="name" onChange={this.handleChange.bind(this, "name")} />
                            </div>

                            <div className="form-group">
                                <label htmlFor="genre">Žanr:</label>
                                <input type="text" className="form-control" id="genre" placeholder="Unesite žanr" name="genre" onChange={this.handleChange.bind(this, "genre")} />
                            </div>

                            <div className="form-group">
                                <label htmlFor="year">Godina:</label>
                                <input type="text" className="form-control" id="year" placeholder="Unesite godinu" name="year" onChange={this.handleChange.bind(this, "year")} />
                            </div>

                            <div className="form-group">
                                <label htmlFor="runningTime">Trajanje:</label>
                                <input type="text" className="form-control" id="runningTime" placeholder="Unesite dužinu filma" name="runningTime" onChange={this.handleChange.bind(this, "runningTime")} />
                            </div>

                            <div className="form-group">
                                <label htmlFor="director">Režiser:</label>
                                <input type="text" className="form-control" id="director" placeholder="Unesite režisera" name="director" onChange={this.handleChange.bind(this, "director")} />
                            </div>

                            <div className="form-group">
                                <label htmlFor="cast">Glumci:</label>
                                <input type="text" className="form-control" id="cast" placeholder="Unesite glumce" name="cast" onChange={this.handleChange.bind(this, "cast")} />
                            </div>

                            <div className="form-group">
                                <label htmlFor="scriptwriters">Scenaristi:</label>
                                <input type="text" className="form-control" id="scriptwriters" placeholder="Unesite scenariste" name="scriptwriters" onChange={this.handleChange.bind(this, "scriptwriters")} />
                            </div>

                            <div className="form-group">
                                <label htmlFor="producers">Producent:</label>
                                <input type="text" className="form-control" id="producers" placeholder="Unesite režisera" name="producers" onChange={this.handleChange.bind(this, "producers")} />
                            </div>

                            <div className="form-group">
                                <label htmlFor="logline">Opis:</label>
                                <input type="text" className="form-control" id="logline" placeholder="Unesite opis filma" name="logline" onChange={this.handleChange.bind(this, "logline")} />
                            </div>

                            <div className="form-group">
                                <label htmlFor="image">Fotografija:</label> <br/>
                                <input type="file" onChange={this.onImageChange} className="" id="group_image"/>
                            </div>
                            <button type="button" onClick={this.handleSubmit} id="submitFilm" className="btn btn-success">Dodajte film</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default AddMovie;