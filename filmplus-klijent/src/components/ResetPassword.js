import React, { Component } from 'react';
import '../css/ResetPassword.css';
import axios from 'axios';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAngleLeft } from "@fortawesome/free-solid-svg-icons";

export default class ResetPassword extends Component {
    constructor(props) {
        super(props);
        this.state = {
          forUser: {
            password: '',
            confirmPassword: ''
          }
      };

      this.onPasswordChange = this.onPasswordChange.bind(this);
      this.onPasswordConfirmChange = this.onPasswordConfirmChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
    }

    checkFormValidity(entity) {
        const validationPasswordRegex = new RegExp("^(?=.*\\d).{4,12}$");

        if (entity.password === '' || !validationPasswordRegex.test(entity.password)) {
            window.alert("Password must contain between 4-12 characters and at least one digit!");
            return false;
        } else if (entity.password !== entity.confirmPassword) {
            window.alert("Passwords do not match!");
            return false;
        }

        return true;
    }

    onPasswordChange(password) {
        const forUser = {
            password: password.target.value,
            confirmPassword: this.state.forUser.confirmPassword
        };

        this.setState({
            forUser
        });
    }

    onPasswordConfirmChange(passwordConfirm) {
        const forUser = {
            password: this.state.forUser.password,
            confirmPassword: passwordConfirm.target.value
        };
        this.setState({
            forUser
        });
    }

    handleSubmit() {
        const forUser = this.state.forUser;

        let valid = this.checkFormValidity(forUser);
        if (!valid) {
            return ;
        }
        const body = {
            password: forUser.password,
            email: localStorage.getItem("email")
        };

        axios.post('http://localhost:3001/resetPassword', body)
        .then((_response) => {
            window.alert("Uspešno ste promenili šifu");
        })
        .catch((error) => {
            console.log(error);
        });

        document.getElementById("resetPassword").reset();
        window.location.href = "/login";

    }

    render() {
        return (
        <div className="resetPassword">
            <div className="formular">

                <div className="back"><a href='/login'><FontAwesomeIcon icon={faAngleLeft} className="fi_menu"/> Vratite se na prijavu</a></div>
                <div className="container">

                    <h2 className="titleResetPassword">Promenite šifru</h2>
                    <form id="resetPassword">
                        <div className="form-group">
                            <label htmlFor="pwd">Šifra:</label>
                            <input type="password" className="form-control" id="pwdUser" placeholder="Unesite šifru" name="pwd" onChange={this.onPasswordChange}/>
                        </div>

                        <div className="form-group">
                            <label htmlFor="pwd">Potvrdite šifru:</label>
                            <input type="password" className="form-control" id="pwdUserConfirm" placeholder="Unesite šifru" name="pwd" onChange={this.onPasswordConfirmChange}/>
                        </div>

                        <button type="button" onClick={this.handleSubmit} id="submitResetPassword" className="btn btn-success">Potvrdite</button>
                        <p className="goToLogin">Nemate svoj nalog? <a href='/register'>Kreirajte ga!</a></p>
                    </form>
                </div>
            </div>
        </div>
    );
  }
}