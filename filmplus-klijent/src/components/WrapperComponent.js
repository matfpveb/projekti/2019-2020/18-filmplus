import React, { Component } from 'react';
import axios from 'axios';
import Profile from './Profile';
import { Redirect } from 'react-router-dom';

class WrapperComponent extends Component {

    constructor(props) {
      super(props);
      this.state = {
        isAuth: true
      };
    }
  
    componentDidMount() {
        let body = {
            secretToken: localStorage.getItem("token")
        };
        axios.post('http://localhost:3001/checkToken', body)
        .then((response) => {
            if(response.data.check === true){
                this.setState({isAuth: true})
            } else {
                this.setState({isAuth: false})
            }
        })
        .catch((error) => {
            console.log(error);
        });
    }
  
    render() {
        if(this.state.isAuth === null) return null;

        return (this.state.isAuth ? <Profile /> : <Redirect to={{pathname: '/login', state: {from: this.props.location}}} />);
    }
  }

export default WrapperComponent;
