import React from 'react';
import '../css/Homepage.css'
import Header from './Header';
import Slideshow from './Slideshow';

class Homepage extends React.Component {

  render() {
    return (
      <div className="homepage">
        <Header page="home"/>
        <Slideshow />
        
      </div>
      );
  }

}

export default Homepage;