import React from 'react';
import Header from './Header';
import '../css/Series.css'
import axios from 'axios';
import Rating from '@material-ui/lab/Rating';
import Box from '@material-ui/core/Box';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';

class Series extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: '',
            name: '',
            genre: '',
            year: '',
            episodes: '',
            seasons: '',
            director: '',
            cast: '',
            scriptwriters: '',
            image: '',
            producers: '',
            logline: '',            
            isAuth: true,
            open: false,
            mark: 0,
            rating_list: []
        };

        this.addSeries = this.addSeries.bind(this);
        this.rateTheSeries = this.rateTheSeries.bind(this);
        this.closeDialog = this.closeDialog.bind(this);
        this.changeMark = this.changeMark.bind(this);
    }

    componentDidMount() {
        let name = this.props.match.params.name;
        let url = `http://localhost:3001/seriesByName/${name}`;
        axios.get(url)
        .then(res => {
            let rating_list = [];
            for (let i = 0; i < res.data[0].rating_list.length; i++) {
                if (i === res.data[0].rating_list.length - 1) {
                    rating_list.push(<span key={i}>{res.data[0].rating_list[i].mark} </span>);
                } else {
                    rating_list.push(<span key={i}>{res.data[0].rating_list[i].mark}, </span>);
                }
            }
            this.setState({
                id: res.data[0]._id,
                image: '/' + res.data[0].image,
                name: res.data[0].name,
                genre: res.data[0].genre,
                year: res.data[0].year,
                episodes: res.data[0].episodes,
                seasons: res.data[0].seasons,
                director: res.data[0].director,
                cast: res.data[0].cast,
                scriptwriters: res.data[0].scriptwriters,
                producers: res.data[0].producers,
                logline: res.data[0].logline,
                rating_list: rating_list
            })
        });
        let body = {
            secretToken: localStorage.getItem("token")
        };
        axios.post('http://localhost:3001/checkToken', body)
        .then((response) => {
            this.setState({isAuth: response.data.check});
        })
        .catch((error) => {
            console.log(error);
        });
    }
    addSeries() {
        const email = localStorage.getItem("email");
        const id = this.state.id;
        const body = {
            email: email,
            id: id
        };

        axios.post('http://localhost:3001/addSeriesForUser', body)
        .then((_res) => {
            window.alert("Dodali ste seriju " + this.props.match.params.name + " u vašu listu serija!")
        })
        .catch((error) => {
            window.alert("Seriju " + this.props.match.params.name + " ste već dodali u vašu listu serija!")
            console.log(error);
        });
    }

    rateTheSeries() {
        this.setState({
            open: true
        });
    }

    closeDialog() {
        this.setState({
            open: false
        });
    }

    changeMark(event) {
        this.setState({
            mark: event.target.value,
            open: false
        });
        const email = localStorage.getItem("email");
        const body = {
            email: email,
            id: this.state.id,
            mark: event.target.value
        };
        axios.post('http://localhost:3001/rateTheSeries', body)
        .then((_res) => {
            window.alert("Hvala vam što ste ocenili serija "  + this.props.match.params.name + "!");
            window.location.reload();
        })
        .catch((_error) => {
            window.alert("Seriju " + this.props.match.params.name + " ste već ocenili!")
        });
    }

    render() {
        return (
        <div className="seria" id="seria">
            <Header />
            <div className="seriaCenter">
                <h1 className="titleSeriesResponse">{this.state.name}</h1>
                <div className="imgSeria">
                    <img src={this.state.image} alt=''></img>
                </div>
                <div className="infoSeria">
                    <h1 className="titleSeria">{this.state.name}</h1>
                    <p>ŽANR: {this.state.genre}</p> <br/>
                    <p>GODINA: {this.state.year}</p> <br/>
                    <p>BROJ EPIZODA: {this.state.episodes}</p><br/>
                    <p>BROJ SEZONA: {this.state.seasons}</p><br/>
                    <p>REŽISER: {this.state.director}</p><br/>
                    <p>GLUMCI: {this.state.cast}</p><br/>
                    <p>SCENARISTA: {this.state.scriptwriters}</p><br/>
                    <p>PRODUCENT: {this.state.producers}</p><br/>
                    <p>OPIS: {this.state.logline}</p><br/>
                    <p>Ocene: {this.state.rating_list}</p><br/>
                    <div className="buttonsMovies">
                        {this.state.isAuth ?
                            <button type="button" onClick={this.addSeries} id="submitLogin" className="btn btn-success">Dodajte seriju</button> :
                            ''
                        }
                        {this.state.isAuth ?
                            <button type="button" onClick={this.rateTheSeries} id="rateTheMovie" className="btn btn-success">Ocenite seriju</button> :
                            ''
                        }
                    </div>
                </div>
            </div>
            <div>
                <Dialog
                    open={this.state.open}
                    onClose={this.closeDialog}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                >
                    <DialogTitle id="alert-dialog-title">{"OCENITE FILM"}</DialogTitle>
                    <DialogContent>
                        <DialogContentText id="alert-dialog-description">
                            Hvala što ste ocenili film!
                        </DialogContentText>
                    </DialogContent>   
                    <Rating
                        name="hover-feedback"
                        id="rating"
                        onChange={this.changeMark}
                        defaultValue={2}
                        precision={0.5}
                    />
                    {<Box ml={2}></Box>}

                </Dialog>
            </div>
        </div>
        );
    }

}

export default Series;