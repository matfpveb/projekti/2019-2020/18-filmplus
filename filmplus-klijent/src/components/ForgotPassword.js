import React, { Component } from 'react';
import '../css/ForgotPassword.css';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAngleLeft } from "@fortawesome/free-solid-svg-icons";
import axios from 'axios';

export default class ForgotPassword extends Component {
    constructor(props) {
        super(props);
        this.state = {
          forUser: {
              email: ''
          }
        };

      this.onEmailChange = this.onEmailChange.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
    }

    checkValidityOfEmail(entity) {
        const validationEmailRegex = new RegExp("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$");

        if (entity.email === '' || !validationEmailRegex.test(entity.email)) {
            window.alert("Please insert valid email!");
            return false;
        } 

        return true;
    }

    onEmailChange(email) {
        const forUser = {
            email: email.target.value
        };

        this.setState({
            forUser
        });
    }

    handleSubmit() {
        const forUser = this.state.forUser;

        let valid = this.checkValidityOfEmail(forUser);
        if (!valid) {
            return ;
        }
        const body = {
            email: forUser.email
        };
        localStorage.setItem("email", forUser.email);

        axios.post('http://localhost:3001/forgotPassword', body)
        .then((_response) => {
        })
        .catch((error) => {
            console.log(error);
        });

        document.getElementById("forgotPassword").reset();
    }

    render() {
        return (
        <div className="forgotPassword">
            <div className="formular">

                <div className="back"><a href='/'><FontAwesomeIcon icon={faAngleLeft} className="fi_menu"/> Vratite se na početnu</a></div>
                <div className="container" id="logInUser">

                    <h2 className="titleFormForgotPassword">Promenite šifru</h2>
                    <p>Da biste promenili šifru potrebno je da unesete email sa kojim ste napravili nalog </p>
                    <form id="forgotPassword">
                        <div className="form-group">
                            <label htmlFor="email">Email:</label>
                            <input type="email" className="form-control" id="emailUser" placeholder="Unesite email" name="email" onChange={this.onEmailChange}/>
                        </div>

                        <button type="button" onClick={this.handleSubmit} id="submitForgotPassword" className="btn btn-success">Potvrdite</button>
                        <p className="goToLogin">Nemate svoj nalog? <a href='/register'>Kreirajte ga!</a></p>
                    </form>
                </div>
            </div>
        </div>
    );
  }
}