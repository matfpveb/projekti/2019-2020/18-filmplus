import React from 'react';
import '../css/Footer.css';
import ReturnToTop from 'react-scroll-to-top';

function Footer() {
    return (
        <div className="footer" id="footer">
            <div className="containter">
                <div className="aboutUs">
                    <p> O nama </p>
                    <p> Sajt sa filmove i serije </p>
                </div>
                <div id="titleFooter">
                    <h1 id="filmPlus">FILMPLUS</h1>
                </div>
            </div>

            <div className="copyright">
                <p> Copyright © 2020, All Rights Reserved </p>
                <ReturnToTop />
            </div>
        </div>
    );
}
export default Footer;

