import React from 'react';
import '../css/Profile.css'
import axios from 'axios';
import Header from './Header';

class Profile extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            lastname: '',
            email: '',
            listMovies: [],
            listSeries: [],
            movies: [],
            series: [],
            changePassword: {
                password: '',
                confirmPassword: ''
            }
        };

        this.onPasswordChange = this.onPasswordChange.bind(this);
        this.onPasswordConfirmChange = this.onPasswordConfirmChange.bind(this);        
        this.handleSubmit = this.handleSubmit.bind(this);

    }

    onPasswordChange(password) {
        const changePassword = {
            password: password.target.value,
            confirmPassword: this.state.changePassword.confirmPassword
        };

        this.setState({
            changePassword
        });
    }

    onPasswordConfirmChange(passwordConfirm) {
        const changePassword = {
            password: this.state.changePassword.password,
            confirmPassword: passwordConfirm.target.value
        };
        this.setState({
            changePassword
        });
    }

    handleSubmit() {
        const validationPasswordRegex = new RegExp("^(?=.*\\d).{4,12}$");
        const changePassword = this.state.changePassword;
        if (changePassword.password === '' || !validationPasswordRegex.test(changePassword.password)) {
            window.alert("Password must contain between 4-12 characters and at least one digit!");
            return;
        } else if (changePassword.password !== changePassword.confirmPassword) {
            window.alert("Passwords do not match!");
            return;
        }
        
        const body = {
            password: changePassword.password,
            email: localStorage.getItem("email")
        };

        axios.post('http://localhost:3001/resetPassword', body)
        .then((_response) => {
            window.alert("Uspešno ste promenili šifu")
        })
        .catch((error) => {
            console.log(error);
        });
        window.location.href = "/login";
        
    }

    componentDidMount() {
        let email = localStorage.email;
        axios.post('http://localhost:3001/findUserByEmail', {
            email: email
        })
        .then(res => {
            this.setState({
                name: res.data.name,
                lastname: res.data.lastname,
                email: res.data.email,
                listMovies: res.data.listMovies,
                listSeries: res.data.listSeries,
                active: res.data.active,
                image: res.data.image
            });
            
            let moviesList = [];
            for (let i = 0; i<res.data.listMovies.length; i++) {
                let href = "/movie/" + res.data.listMovies[i].name;
                moviesList.push(<li key={i}><a href={href}>{res.data.listMovies[i].name}</a></li>);
            }

            let movies = [<ul key={1}>{moviesList}</ul>];
            this.setState({
                movies: movies
            });

            let seriesList = [];
            for (let i = 0; i<res.data.listSeries.length; i++) {
                let href = "/series/" + res.data.listSeries[i].name;
                seriesList.push(<li key={i}><a href={href}>{res.data.listSeries[i].name}</a></li>);
            }
            
            let series = [<ul key={2}>{seriesList}</ul>];
            this.setState({
                series: series
            });
        });
    }

    render() {
        return (
        <div className="profile" id="profile">
            <Header active={this.state.active}/>
            <div className="imgOfUser"> 
                <div><img src={this.state.image} alt="user" /></div>
                <div className="nameOfUser">
                    {this.state.name} {this.state.lastname}
                </div>
                <p>{this.state.email}</p>
            </div>
            <div className="infoProfile">
                <div className="listOfMovies">
                    <h2 className="titleProfile">Moji filmovi</h2>
                    {this.state.movies}
                    <a href="/sendMailMovie"><button type="button" id="submitFilm" className="btn btn-success">Predložite film</button></a>
                </div>
                <div className="listOfSeries">
                    <h2 className="titleProfile">Moje serije</h2>
                    {this.state.series}
                    <a href="/sendMailSeries"><button type="button" id="submitSeries" className="btn btn-success">Predložite seriju</button></a>
                </div>
                <div className="passwordChange">
                    <h2 className="titleProfile">Promenite šifru</h2>
                    <div className="form-group">
                        <label htmlFor="pwd">Šifra:</label>
                        <input type="password" className="form-control" id="pwdUser" placeholder="Unesite šifru" name="pwd" onChange={this.onPasswordChange}/>
                    </div>

                    <div className="form-group">
                        <label htmlFor="pwd">Potvrdite šifru:</label>
                        <input type="password" className="form-control" id="pwdUserConfirm" placeholder="Unesite šifru" name="pwd" onChange={this.onPasswordConfirmChange}/>
                    </div>
                    <button type="button" onClick={this.handleSubmit} id="submit" className="btn btn-success">Promenite šifru</button>
                </div>
            </div>
        
        </div>
        );
    }

}

export default Profile;