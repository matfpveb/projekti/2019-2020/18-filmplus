import React, { Component } from 'react';
import axios from 'axios';
import { Redirect } from 'react-router-dom';
import AddSeries from './AddSeries';

class WrapperSecretComponentMovie extends Component {

    constructor(props) {
      super(props);
      this.state = {
        isAuth: true
      };
    }
  
    componentDidMount() {
        let body = {
            email: localStorage.getItem("email")
        };
        axios.post('http://localhost:3001/isAdmin', body)
        .then((response) => {
            if(response.data.check === true){
                this.setState({isAuth: true})
            } else {
                this.setState({isAuth: false})
            }
        })
        .catch((error) => {
            console.log(error);
        });
    }
  
    render() {
        if(this.state.isAuth === null) return null;

        return (this.state.isAuth ? <AddSeries /> : <Redirect to={{pathname: '/', state: {from: this.props.location}}} />);
    }
  }

export default WrapperSecretComponentMovie;
