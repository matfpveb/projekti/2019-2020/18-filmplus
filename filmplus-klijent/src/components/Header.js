import React from 'react';
import '../css/Header.css'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBars } from "@fortawesome/free-solid-svg-icons";


class Header extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            buttons: []
        };
        this.logout = this.logout.bind(this);
    }

    handleChange({target}){
        let nb = document.getElementById('NB_css_response');
        if (target.checked){
          nb.style.display = "block";
        } else {
          nb.style.display = "none";
        }
    }

    componentDidMount() {
        let header = document.getElementById("header");
        if (this.props.page === "home") {
            header.style.position = "fixed";
        } else {
            header.style.position = "relative";
        }
    }

    logout() {
        localStorage.setItem('token', '');
        localStorage.setItem('email', '');
    }

  render() {
    let buttons = [];
    let token = localStorage.getItem('token');
    let linkResponse = [];
    if (token !== '') {
        buttons.push(<a key={1} href="/profile"><button className="btnLogIn"> <span className="textBtn">Moj profil</span></button> </a>);
        buttons.push(<a key={2} href="/"><button className="btnSignIn" onClick={this.logout}> <span className="textBtn">Odjavite se</span></button> </a>);
        linkResponse.push(<li key={5}> <a href="/profile"> MOJ PROFIL </a></li>);
        linkResponse.push(<li key={6}> <a href="/" onClick={this.logout}>ODJAVITE SE</a></li>);
        
    } else {
        buttons.push(<a key={3} href="/login"><button className="btnLogIn"> <span className="textBtn">Prijavite se</span></button> </a>);
        buttons.push(<a key={4} href="/register"> <button className="btnSignIn">Registrujte se</button> </a>);
        linkResponse.push(<li key={7}> <a href="/login">PRIJAVITE SE </a></li>);
        linkResponse.push(<li key={8}> <a href="/register">REGISTRUJTE SE</a></li>);
    }
    
    buttons = (<div className="buttonsHeader"> {buttons} </div>);
    
    return (
        <div>
            <div className="header" id="header">
                <div className="logo_header">
                    <h2>FILMPLUS</h2>
                </div>

                <div className="NB_css">
                    <ul>
                        <li>
                            <a href="/"> POČETNA </a>
                        </li>
                        <li>
                            <a href="/movies"> FILMOVI </a> 
                        </li>
                        <li>
                            <a href="/series"> SERIJE </a>
                        </li>
                        <li>
                            <a href="/team"> TIM </a>
                        </li>
                    </ul>
                

                </div>
                {buttons}
        </div>
        <div className="header_response">
            <div className="linija_header_response">
                <h2>FILMPLUS</h2>
                <div className="fa_bars">
                    <label htmlFor="check" className="checkbtn">
                        <input type="checkbox" id="check" onClick={this.handleChange}
                                    defaultChecked={this.props.complete}/>
                        <FontAwesomeIcon icon={faBars} className="fa_bars_icon"/>
                    </label>
                </div>
            </div>

            <div className="NB_css_response" id="NB_css_response">
                <ul>
                    <li>
                        <a href="/"> POČETNA </a>
                    </li>
                    <li>
                        <a href="/movies"> FILMOVI </a> 
                    </li>
                    <li>
                        <a href="/series"> SERIJE </a>
                    </li>
                    <li>
                        <a href="/team"> TIM </a>
                    </li>
                    {linkResponse}
                </ul>
            </div>
        </div>
    </div>
      );
  }

}

export default Header;