import React from 'react';
import Header from './Header';
import '../css/Team.css'
import Footer from './Footer';

class Team extends React.Component {
    
    render() {
        return (
        <div className="team">
            <Header />
            <div id="tim">
                    <h1 id="titleTim">TIM</h1>
                    <div className="clanovi_timovi">
                        <div className="clan_t"> 
                            <div> <img src="/andjela.jpg" alt="clan" /> </div>
                            <div> Andjela Križan </div>
                        </div>
                        <div className="clan_t"> 
                            <div> <img src="/vera.jpg" alt="clan" /> </div>
                            <div> Vera Milosavljević </div>
                        </div>
                        <div className="clan_t"> 
                            <div> <img src="/igor.jpg" alt="clan" /> </div>
                            <div> Igor Mandić </div>
                        </div>
                        <div className="clan_t"> 
                            <div> <img src="/petar.jpg" alt="clan" /> </div>
                            <div> Petar Pejović </div>
                        </div>
                    </div>
                </div>
                <div id="map">
                    <iframe 
                        className="map"
                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2830.080601058577!2d20.45651345139422!3d44.819922584118345!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x475a7ab4a72f8975%3A0x7c9c68a37ad79aad!2sFaculty%20of%20Mathematics!5e0!3m2!1sen!2srs!4v1600985855965!5m2!1sen!2srs"
                        width="100%" 
                        height="600px" 
                        frameBorder="0" 
                        allowFullScreen=""
                        aria-hidden="false" 
                        tabIndex="0"
                        title="mapa"
                    />
                </div>
            <Footer />
        </div>
        );
    }

}

export default Team;