const express = require('express');
const router = express.Router();
const controllerUser = require('../../controllers/users');
const controllerMovies = require('../../controllers/movies');
const controllerSeries = require('../../controllers/series');
const upload = require('../../middleware/uploadImage')

router.post('/newUser', upload.single('image'), controllerUser.addNewUser);
router.post('/sendVerifyToken', controllerUser.sendVerifyToken);
router.post('/verify', controllerUser.verify);
router.post('/signin', controllerUser.findUser);
router.post('/findUserByEmail', controllerUser.findUserByEmail);
router.post('/addMovieForUser', controllerUser.addMovieForUser);
router.post('/addSeriesForUser', controllerUser.addSeriesForUser);
router.post('/checkToken', controllerUser.checkToken);
router.post('/getListOfMovies', controllerUser.getListOfMovies);
router.post('/resetPassword', controllerUser.resetPassword);
router.post('/forgotPassword', controllerUser.forgotPassword);
router.post('/isAdmin', controllerUser.isAdmin);
router.get('/users', controllerUser.listUsers);

router.post('/saveMovie', upload.single('image'), controllerMovies.saveMovie);
router.get('/moviesByName/:name', controllerMovies.findMovieByName);
router.get('/moviesByActor/:actor', controllerMovies.findMoviesByActor);
router.get('/movies', controllerMovies.listAllMovies);
router.post('/listOfMovies', controllerMovies.listOfMovies);
router.get('/moviesByGenre/:genre', controllerMovies.listAllMoviesByGenre);
router.get('/moviesByDirector/:director', controllerMovies.listAllMoviesByDirector);
router.get('/moviesByYear/:year', controllerMovies.listAllMoviesByYear);
router.post('/rateTheMovie', controllerMovies.rateTheMovie);
router.post('/sendMailForMovie', controllerMovies.sendMailForMovie);
router.get('/listOfGenre', controllerMovies.listOfGenre);
router.get('/listOfDirectors', controllerMovies.listOfDirector);

router.post('/saveSeries', upload.single('image'), controllerSeries.saveSeries);
router.get('/seriesByName/:name', controllerSeries.findSeriesByName);
router.get('/seriesByActor/:actor', controllerSeries.findSeriesByActor);
router.get('/series', controllerSeries.listAllSeries);
router.post('/listOfSeries', controllerSeries.listOfSeries);
router.get('/seriesByGenre/:genre', controllerSeries.listAllSeriesByGenre);
router.get('/seriesByDirector/:director', controllerSeries.listAllSeriesByDirector);
router.get('/seriesByYear/:year', controllerSeries.listAllSeriesByYear);
router.post('/rateTheSeries', controllerSeries.rateTheSeries);
router.post('/sendMailForSeries', controllerSeries.sendMailForSeries);
router.get('/serieslistOfGenre', controllerSeries.listOfGenre);
router.get('/serieslistOfDirectors', controllerSeries.listOfDirector);

module.exports = router;
