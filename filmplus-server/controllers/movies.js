const Movie = require('../models/movies');
const User = require('../models/users');
const mongoose = require('mongoose');
const nodemailer = require('nodemailer');

let saveMovie = async (req, res, next) => {
    try {
        const name = req.body.name;
        const genre = req.body.genre;
        const year = req.body.year;
        const runningTime = req.body.runningTime;
        const director = req.body.director;
        const cast = req.body.cast;
        const scriptwriters = req.body.scriptwriters;
        const producers = req.body.producers;
        const logline = req.body.logline;

        if (!genre || !name || !year || !runningTime || 
            !director || !cast || !scriptwriters ||
            !producers || !logline) {
                res.status(400).send("Bad request");
                return;
        }

        let movie = await Movie.find({
            name: name
        });
        if (movie.length === 0) {
            const newMovie = new Movie({
                _id: new mongoose.Types.ObjectId,
                name,
                genre,
                year,
                runningTime,
                director,
                cast,
                scriptwriters,
                image: req.file.filename,
                producers,
                logline,
                rating_list: []
            });

            await newMovie.save();
        
            res.status(201).send("Uspešno ste dodali novi film!");
        } else {
            res.send("Film već postoji");
            res.status(409);
        }
    } catch (error) {
        next(error);
    }
}

let findMovieByName = async(req, res, next) => {
    try {
        let movies = [];
        const name = req.params.name;
        if (name !== 'no') {
            movies = await Movie.find({
                name: new RegExp('.*'+name+'.*')
            });
        } else {
            movies = await Movie.find({});
        }

        if (movies.lennth === 0) {
            res.send("Movie not found!").status(404);
            return;
        } else {
            res.status(200).json(movies);
        }
    } catch (error) {
        next(error)
    }
}

let findMoviesByActor = async(req, res, next) => {
    try {
        let movies = [];
        const actor = req.params.actor;
        if (actor !== 'no') {
            movies = await Movie.find({
                cast: new RegExp('.*'+actor+'.*')
            });
        } else {
            movies = await Movie.find({});
        }

        if (movies.lennth === 0) {
            res.send("Movie not found!").status(404);
            return;
        } else {
            res.status(200).json(movies);
        }
    } catch (error) {
        next(error)
    }
}

let listAllMovies = async(_req, res, next) => {
    try {
        let movies = await Movie.find({});
        if (movies.length === 0) {
            res.status(404);
            res.json({msg:"No movies were found."});
            return;
        } else {
            res.status(200).json(movies);
        }
    } catch (error) {
        next(error);
    }
}


let listOfMovies = async(req, res, next) => {
    try {
        let genre = req.body.genre;
        let director = req.body.director;
        let yearStart = req.body.yearStart;
        let yearEnd = req.body.yearEnd;
        let movies = [];
        if (genre !== 'no' && director !== 'no') {
            movies = await Movie.find({
                genre: genre,
                director: director,
                year: {
                    $gte: yearStart,
                    $lt: yearEnd
                }
            });
        } else if (genre !== 'no') {
            movies = await Movie.find({
                genre: genre,
                year: {
                    $gte: yearStart,
                    $lt: yearEnd
                }
            });
        } else if (director !== 'no') {
            movies = await Movie.find({
                director: director,
                year: {
                    $gte: yearStart,
                    $lt: yearEnd
                }
            });
        } else {
            movies = await Movie.find({
                year: {
                    $gte: yearStart,
                    $lt: yearEnd
                }
            });
        }

        if (movies.length === 0) {
            res.status(404);
            res.json({msg:"No movies were found."});
            return;
        } else {
            res.status(200).json(movies);
        }


    } catch (error) {
        next(error);
    }
}
let listAllMoviesByGenre = async(req, res, next)=>{
    try {
        const genre = req.params.genre;
        let movies = [];
        if (genre !== 'no') {
            movies = await Movie.find({
                genre: genre
            });
        } else {
            movies = await Movie.find({});
        }

        if (movies.length === 0) {
            res.send("No movies were found.").status(404);
            return;
        } else {
            res.status(200).json(movies);
        }
    } catch (error) {
        next(error);
    }
}

let listAllMoviesByDirector = async(req, res, next)=>{
    try {
        const director = req.params.director;
        let movies = [];
        if (director !== 'no') {
            movies = await Movie.find({
                director: director
            });
        } else {
            movies = await Movie.find({});
        }

        if (movies.length === 0) {
            res.send("No movies were found.").status(404);
            return;
        } else {
            res.status(200).json(movies);
        }
    } catch (error) {
        next(error);
    }
}

let listAllMoviesByYear = async(req, res, next)=>{
    try {
        const year = req.params.year;
        let movies = await Movie.find({year:year});
        if (movies.length === 0) {
            res.send("No movies were found.").status(404);
            return;
        } else {
            res.json(movies).status(200);
        }
    } catch (error) {
        next(error);
    }
}

let rateTheMovie = async(req, res, next)=>{
    try {
        const email = req.body.email;
        const user = await User.findOne({
            email: email
        });

        const id = req.body.id;
        const movie = await Movie.findById(id);
        const marks = movie.rating_list;
        for (let i = 0; i < marks.length; i++) {
            if (user._id.equals(marks[i].id)) {
                res.status(409).send("Film je već ocenjen");
                return;
            }
        }
        await movie.updateOne(
            {
                $push: {
                    rating_list: {
                        mark: req.body.mark,
                        id: user._id
                    }
                }
            }
        );
        res.json(movie).status(200);


    } catch (error) {
        next(error);
    }
}

let sendMailForMovie = async(req, res, next) => {
    try {
        let transporter = nodemailer.createTransport({
            service: 'gmail',
            type: "SMTP",
            host: "smtp.gmail.com",
            secure: true,
            auth: {
              user: 'filmplus.office@gmail.com',
              pass: 'jafakeks'
            }
        });

        transporter.sendMail({
            from: req.body.email,
            to: "filmplus.office@gmail.com",
            subject: "Predlog za film",
            html: "<h1>" + req.body.title + "</h1>" + req.body.html + "<br>Email: " + req.body.email
        }, function (error, _response) {
            if (error) {
                console.log(error);
                res.status(400).send("error");
            } else {
                res.status(200).send("sent");
            }
        });
    } catch (error) {
        next(error);
    }
    
}

let listOfGenre = async (_req, res, next) => {
    try {
        const genre = await Movie.distinct('genre');
        res.status(200).json(genre);
    } catch (error) {
        next(error);
    }
}

let listOfDirector = async (_req, res, next) => {
    try {
        const director = await Movie.distinct('director');
        res.status(200).json(director);
    } catch (error) {
        next(error);
    }
}


module.exports = {
    findMovieByName,
    findMoviesByActor,
    listAllMovies,
    listOfMovies,
    listAllMoviesByGenre,
    listAllMoviesByDirector,
    listAllMoviesByYear,
    saveMovie,
    rateTheMovie,
    sendMailForMovie,
    listOfGenre,
    listOfDirector
}