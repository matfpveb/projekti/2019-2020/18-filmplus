const User = require('../models/users');
const Series = require('../models/series');
const Movie = require('../models/movies');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const mongoose = require('mongoose');
const randomstring = require('randomstring');
const nodemailer = require('nodemailer');

let secretToken;

const smtpTransport = nodemailer.createTransport({    
    service: "Gmail",
    auth: {
        user: "filmplus.office@gmail.com",
        pass: "jafakeks"
    }
});

let mailOptions;

let sendVerifyToken = async(req, res, next) => {
    try {
        secretToken = randomstring.generate();
        mailOptions = {
            to: req.body.email,
            subject: "Potvrda naloga",
            html: "Zdravo,<br> Unesite naredni token kako biste završili registraciju.<br><p>" + secretToken + "</p>"
        };

        smtpTransport.sendMail(mailOptions, function (error, _response) {
            if (error) {
                res.status(400).send("error");
            } else {
                res.status(200).send("sent");
            }
        });
    } catch (error) {
        next(error);
    }
}

let forgotPassword = async(req, res, next) => {
    try {
        const user = await User.findOne({email: req.body.email});
        if (!user) {
            return res.status(400).json({
                message: "Email ne postoji!"
            });
        }
        let mail = {
            to: req.body.email,
            subject: "Promena šifre",
            html: "Zdravo,<br> Kliknite na naredni link kako biste promenili šifru.<br><p>" + "http://localhost:3000/resetPassword/" + user.secretToken + "</p>"
        };

        smtpTransport.sendMail(mail, function (error, _response) {
            if (error) {
                res.status(400).send("error");
            } else {
                res.status(200).send("sent");
            }
        });
    } catch (error) {
        next(error);
    }
   
}

let addNewUser = async(req, res, next) => {
    try {
        let image = '';
        if (req.file) {
            image = req.file.filename;
        } else {
            image = 'noProfileImg.jpg';
        }
        const newUser = {
            _id: new mongoose.Types.ObjectId,
            name: req.body.name,
            lastname: req.body.lastname,
            email: req.body.email,
            password: req.body.password,
            secretToken: '',
            active: false,
            image: image,
            listMovies: [],
            listSeries: [],
            admin: false
        };
        const user = await User.findOne({
            email: req.body.email
        });
        if (!user) {
            bcrypt.hash(req.body.password, 10, (_err, hash) => {
                newUser.password = hash;
                newUser.secretToken = secretToken;
                User.create(newUser)
                    .then(user => {
                        res.status(200).json({msg: user.email + ' Registered!'});
                    })
                    .catch(err => {
                        res.send('error: ' + err);
                    })
            });
        } else {
            res.status(409).json({error: "User already exists"});
        }

    } catch (error) {
        next(error);
    }
}

let isAdmin = async(req, res, next) => {
    try {
        const user = await User.findOne({
            email: req.body.email
        });
        if (!user) {
            return res.status(400).json({
                message: "Email ne postoji!"
            });
        }

        if (user.admin) {            
            res.json({
                check: true
            }).status(200);
        } else {
            res.json({
                check: false
            }).status(404);
            }

    } catch (error) {
        next(error);
    }
}

let verify = async(req, res, next) => {
    try {
        const user = await User.findOne({email: req.body.email});
        if (!user) {
            return res.status(400).json({
                message: "Email ne postoji!"
            });
        }

        if (user.secretToken === req.body.secretToken) {            
            await User.updateOne (
                { email: req.body.email },
                {  
                    $set: { active: true }
                });
            
            const path = "/profile/";
            return res.json({
                path: path
            });    

        } else {
            return res.status(401).json({
                message: "Pogrešan verifikacioni kod!"
            });
        }

    } catch (error) {
        next(error);
    }
}

let resetPassword = async(req, res, next) => {
    try {
        secretToken = randomstring.generate();
        bcrypt.hash(req.body.password, 10, (_err, hash) => {
            User.updateOne (
                { email: req.body.email },
                {  
                    $set: { 
                        password: hash,
                        secretToken: secretToken
                    }
                }
            )
            .then((response) => {
                console.log(response);
            })
            .catch((error) => {
                console.log(error)
            });
        });
        res.status(200).send("Password change");
    } catch (error) {
        next(error);
    }

}

let listUsers = async(_req, res, next) => {
    try {
        const users = await User.find({});
        if(users.length === 0){
            res.send("Not found.").status(404);
            return;
        }
        else{
            res.json(users).status(200);
        }
        
    } catch (error) {
        next(error);
    }
}

let findUserByEmail = async(req, res, next) => {
    try {
        const user = await User.findOne({
            email: req.body.email
        });

        if (user) {
            res.status(200).json(user);
        } else {
            res.status(309).send("Korisnik sa ovim email-om ne postoji!");
        }
    } catch (error) {
        next(error);
    }
}

let findUser = async(req, res, next) => {
    try {
        const user = await User.findOne({
            email: req.body.email
        });

        if(user) {
            if (bcrypt.compareSync(req.body.password, user.password)) {
                const payload = {
                    _id: user._id,
                    email: user.email
                };

                let token = jwt.sign(payload, 'kupus', {
                    expiresIn: 1440
                });

                res.json({
                    token: token,
                    name: user.name,
                    lastname: user.lastname,
                    email: user.email,
                    username: user.username,
                    listMovies: user.listMovies,
                    listSeries: user.listSeries,
                    secretToken: user.secretToken
                }).status(200);
                
            } else {
                res.status(404).json({
                    error: "User does not exist"
                }); 
            }
        } else {
            res.status(404).json({
                error: "User does not exist"
            });
        }

    } catch (error) {
        next(error);
    }
}

let addMovieForUser = async(req, res, next) =>{
    try {
        const email = req.body.email;
        const user = await User.findOne({
            email: email
        });
        if(!user){
            res.send("User not found").status(404);
        }
        const newMovie = await Movie.findById(req.body.id);
        for (let i = 0; i<user.listMovies.length; i++) {
            if (user.listMovies[i].id == req.body.id) {
                res.status(409).send("Film je već dodat");
                return;
            }
        }
        await user.updateOne(
            {
                $push: {
                    listMovies: {
                        name: newMovie.name,
                        id: newMovie._id,
                        mark: false
                    }
                }
            }
        );
        res.json(user).status(200);

    } catch (error) {
        next(error);
    }
}

let addSeriesForUser = async(req, res, next) => {
    try {
        const email = req.body.email;
        const user = await User.findOne({
            email: email
        });

        if (!user) {
            res.send("User not found").status(404);
        }

        const newSeries = await Series.findById(req.body.id);
        for (let i = 0; i < user.listSeries.length; i++) {
            if (user.listSeries[i].id == req.body.id) {
                res.status(409).send("Serija je već dodata");
                return;
            }
        }
        await user.updateOne(
            {
                $push: {
                    listSeries: {
                        name: newSeries.name,
                        id: newSeries._id,
                        mark: false
                    }
                }
            }
        );

        res.json(user).status(200);

    } catch (error) {
        next(error);
    }
}

let getListOfMovies = async(req, res, next) => {
    try {
        const email = req.body.email;
        const user = await User.findOne({
            email: email
        });

        if (!user) {
            res.status(404).json({
                error: "User does not exist"
            }); 
        }
        const listMovies = user.listMovies;
        res.status(200).json({
            listMovies: listMovies
        });
    } catch (error) {
        next(error);
    }

}

let getListOfSeries = async(req, res, next) => {
    try {
        const email = req.body.email;
        const user = await User.findOne({
            email: email
        });

        if (!user) {
            res.status(404).json({
                error: "User does not exist"
            }); 
        }
        const listSeries = user.listSeries;
        res.status(200).json({
            listSeries: listSeries
        });
    } catch (error) {
        next(error);
    }

}


let checkToken = async(req, res, next) => {
    try {
        const user = await User.findOne({
            secretToken: req.body.secretToken
        });
        if (user) {
            res.json({
                check: true
            }).status(200);
        } else {
            res.json({
                check: false
            }).status(404);
        }

    } catch (error) {
        next(error);
    }
}

module.exports = {
    addNewUser,
    sendVerifyToken,
    verify,
    findUser,
    findUserByEmail,
    addMovieForUser,
    addSeriesForUser,
    listUsers,
    checkToken,
    getListOfMovies,
    getListOfSeries,
    resetPassword,
    forgotPassword,
    isAdmin
}