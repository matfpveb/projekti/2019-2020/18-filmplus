const Series = require('../models/series');
const User = require('../models/users')
const mongoose = require('mongoose');
const nodemailer = require('nodemailer');

let saveSeries = async (req, res, next) => {
    try {
        const name = req.body.name;
        const genre = req.body.genre;
        const year = req.body.year;
        const director = req.body.director;
        const cast = req.body.cast;
        const scriptwriters = req.body.scriptwriters;
        const producers = req.body.producers;
        const seasons = req.body.seasons;
        const episodes = req.body.episodes;
        const logline = req.body.logline;

        if (!genre || !name || !year || !seasons || 
            !director || !cast || !scriptwriters ||
            !producers || !episodes || !logline) {
                res.status(400).send("Bad request");
                return;
        }

        let series = await Series.find({name:name});
        if (series.length === 0) {
            const newSeries = new Series({
                _id: new mongoose.Types.ObjectId,
                name,
                genre,
                year,
                seasons,
                episodes,
                director,
                cast,
                scriptwriters,
                image: req.file.filename,
                producers,
                logline,
                rating_list: []
            });

            await newSeries.save();
        
            res.status(201).send("Uspešno ste dodali novu seriju!");
        } else {
            res.send("Serija već postoji");
            res.status(409);
        }
    } catch (error) {
        next(error);
    }
}

let findSeriesByName = async(req, res, next) => {
    try {
        let series = [];
        const name = req.params.name;
        if (name !== 'no') {
            series = await Series.find({
                name: new RegExp('.*'+name+'.*')
            });
        } else {
            series = await Series.find({});
        }

        if (series.lennth === 0) {
            res.send("Series not found!").status(404);
            return;
        } else {
            res.status(200).json(series);
        }
    } catch (error) {
        next(error)
    }
}

let findSeriesByActor = async(req, res, next) => {
    try {
        let series = [];
        const actor = req.params.actor;
        if (actor !== 'no') {
            series = await Series.find({
                cast: new RegExp('.*'+actor+'.*')
            });
        } else {
            series = await Series.find({});
        }

        if (series.lennth === 0) {
            res.send("Series not found!").status(404);
            return;
        } else {
            res.status(200).json(series);
        }
    } catch (error) {
        next(error)
    }
}

let listOfSeries = async(req, res, next) => {
    try {
        let genre = req.body.genre;
        let director = req.body.director;
        let yearStart = req.body.yearStart;
        let yearEnd = req.body.yearEnd;
        let series = [];
        if (genre !== 'no' && director !== 'no') {
            series = await Series.find({
                genre: genre,
                director: director,
                year: {
                    $gte: yearStart,
                    $lt: yearEnd
                }
            });
        } else if (genre !== 'no') {
            series = await Series.find({
                genre: genre,
                year: {
                    $gte: yearStart,
                    $lt: yearEnd
                }
            });
        } else if (director !== 'no') {
            series = await Series.find({
                director: director,
                year: {
                    $gte: yearStart,
                    $lt: yearEnd
                }
            });
        } else {
            series = await Series.find({
                year: {
                    $gte: yearStart,
                    $lt: yearEnd
                }
            });
        }

        if (series.length === 0) {
            res.status(404);
            res.json({msg:"No series were found."});
            return;
        } else {
            res.status(200).json(series);
        }


    } catch (error) {
        next(error);
    }
}

let listAllSeries = async(_req, res, next)=>{
    try {
        const series = await Series.find({});
        if (series.length === 0) {
            res.status(404);
            res.send("Not found.");
            return;
        } else {
            res.json(series).status(200);
        }
    } catch (error) {
        next(error);
    }
}

let listAllSeriesByDirector = async(req, res, next)=>{
    try {
        const director = req.params.director;
        let series = [];
        if (director !== 'no') {
            series = await Series.find({
                director: director
            });
        } else {
            series = await Series.find({});
        }
        if (series.length === 0) {
            res.send("No series were found.").status(404);
            return;
        } else {
            res.status(200).json(series);
        }
    } catch (error) {
        next(error);
    }
}

let listAllSeriesByGenre = async(req, res, next)=>{
    try {
        const genre = req.params.genre;
        let series = await Series.find({genre:genre});
        if (series.length === 0) {
            res.status(404);
            res.send("Not found.");
            return;
        } else {
            res.json(series).status(200);
        }
    } catch (error) {
        next(error);
    }
}

let listAllSeriesByYear = async(req, res, next)=>{
    try {
        const year = req.params.year;
        let series = await Series.find({year:year});
        if (series.length === 0) {
            res.status(404);
            res.send("Not found.");
            return;
        } else {
            res.json(series).status(200);
        }
    } catch (error) {
        next(error);
    }
}

const smtpTransport = nodemailer.createTransport({    
    service: "Gmail",
    auth: {
        user: "filmplus.office@gmail.com",
        pass: "jafakeks"
    }
});

let sendMailForSeries = async(req, res, next) => {
    try {
        let mail = ({
            from: req.body.email,
            to: "filmplus.office@gmail.com",
            subject: "Predlog za seriju",
            html: req.body.title + "<br>" + req.body.html
        });
        
        smtpTransport.sendMail(mail, function (error, _response) {
            if (error) {
                res.status(400).send("error");
            } else {
                res.status(200).send("sent");
            }
        });
    } catch (error) {
        next(error);
    }
}

let rateTheSeries = async(req, res, next)=>{
    try {
        const email = req.body.email;
        const user = await User.findOne({
            email: email
        });

        const id = req.body.id;
        const serie = await Series.findById(id);
        const marks = serie.rating_list;
        for (let i = 0; i < marks.length; i++) {
            if (user._id.equals(marks[i].id)) {
                res.status(409).send("Serija je već ocenjen");
                return;
            }
        }
        await serie.updateOne(
            {
                $push: {
                    rating_list: {
                        mark: req.body.mark,
                        id: user._id
                    }
                }
            }
        );
        res.json(serie).status(200);


    } catch (error) {
        next(error);
    }
}

let listOfGenre = async (_req, res, next) => {
    try {
        const genre = await Series.distinct('genre');
        res.status(200).json(genre);
    } catch (error) {
        next(error);
    }
}

let listOfDirector = async (_req, res, next) => {
    try {
        const director = await Series.distinct('director');
        res.status(200).json(director);
    } catch (error) {
        next(error);
    }
}

module.exports = {
    saveSeries,
    findSeriesByName,
    findSeriesByActor,
    listOfSeries,
    listAllSeries,
    listAllSeriesByDirector,
    listAllSeriesByGenre,
    listAllSeriesByYear,
    sendMailForSeries,
    rateTheSeries,
    listOfGenre,
    listOfDirector
}