const mongoose = require('mongoose');

const seriesSchema = new mongoose.Schema({
    _id : mongoose.Schema.Types.ObjectId,
    name : {
        type : String,
        require : true
    },
    image: {
        type: String,
        require: true
    },
    genre : {
        type : String,
        require : true
    },
    year : {
        type : String,
        require : true
    },
    director : {      // REZISER
        type : String,
        require : true
    },
    cast : {
        type : String,
        require : true
    },
    rating_list : {
        type : [Object],
        require : true
    },
    scriptwriters : {   // SCENARISTI
        type : String,
        require : false
    },
    producers : {     // PRODUCENTI
        type : String,
        require : false
    },
    seasons : {
        type : String,
        require : true
    },
    episodes : {
        type : String,
        require : true
    },
    logline : {  // u jednoj recenici o cemu se radi
        type : String,
        require : false
    }

});

const seriesModel = mongoose.model('Series', seriesSchema);
module.exports = seriesModel;
