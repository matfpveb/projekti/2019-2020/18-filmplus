const mongoose = require('mongoose');

const movieSchema = new mongoose.Schema({
    _id : mongoose.Schema.Types.ObjectId,
    name : {
        type : String,
        require : true
    },
    image: {
        type: String,
        require: true
    },
    genre: {
        type : String,
        require : true
    },
    year: {
        type : String,
        require : true
    },
    runningTime : {
        type : String,
        require : true    
    },
    director: {      // REZISER
        type : String,
        require : true
    },
    cast: {
        type : String,
        require : true
    },
    rating_list : {
        type : [Object],
        require : true
    },
    scriptwriters : {   // SCENARISTI
        type : String,
        require : false
    },
    producers : {     // PRODUCENT
        type : String,
        require : false
    },
    logline : {  // u jednoj recenici o cemu se radi
        type : String,
        require : false
    }

});

const movieModel = mongoose.model('Movie', movieSchema);
module.exports = movieModel;
