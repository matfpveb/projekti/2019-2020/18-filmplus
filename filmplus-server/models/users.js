const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: {
        type: String,
        require: true
    },
    image: {
        type: String,
        require: true
    },
    secretToken: {
        type: String,
        require: true
    },
    active: {
        type: Boolean,
        require: true
    },
    lastname: {
        type: String,
        require: true
    }, 
    email: {
        type: String,
        require: true
    }, 
    password: {
        type: String,
        require: true
    },
    listMovies: {
        type: [Object],
        require: true
    },
    listSeries: {
        type: [Object],
        require: true
    },
    admin: {
        type: Boolean,
        require: true
    }
});

const User = mongoose.model('User', userSchema);
module.exports = User;